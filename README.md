sbup
======
Quick update SlackBuilds/info meant for [SBo](https://slackbuilds.org/). It updates version, download, and md5 fields.

Usage
-----
Run `sbup [version]` in a directory with the info file (and SlackBuild) that you want to update, where `[version]` is the new version to update to. If you have [sbuild](https://gitlab.com/oshd/sbuild) installed it will offer to run a test build (using `sudo`). Options:

- `-i` to build and install the updated SlackBuild (requires `sbuild`).

Screencast
----------
[![asciicast](https://asciinema.org/a/361373.png)](https://asciinema.org/a/361373)

Installation
------------
Put `sbup` in your `PATH` somewhere.
